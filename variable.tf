//Region, ami's y key names:

//variable "aws_access_key" {}
//variable "aws_secret_key" {}

variable "aws_key_path" {}
variable "aws_key_name" {}

//Variables para VPC

variable "vpc_cidr" {
    description = "VPC Elenas"
    default = "10.0.0.0/16"   
}

variable "private_subnet_cidr" {
    description = "CIDR for the Private Subnet 1"
    default = "10.0.1.0/24"
}
variable "public_subnet_cidr" {
    description = "CIDR for the Private Subnet 2"
    default = "10.0.2.0/24"
}



// TAGS

variable "responsable" {
    default = "elena"
}

variable "aws_region" {
	description = "AWS Region to launch servers."
	default		= "us-east-1"
}
