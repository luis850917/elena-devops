# This data source is included for ease of sample architecture deployment
# and can be swapped out as necessary.
data "aws_availability_zones" "available" {}

resource "aws_vpc" "vpc_elena" {
  cidr_block = "${var.private_subnet_cidr}"
}

resource "aws_subnet" "elena_ecs" {
  count = 2
  availability_zone = "${data.aws_availability_zones.available.names[count.index]}"
  cidr_block        = "10.0.${count.index}.0/24"
  vpc_id            = "${aws_vpc.vpc_elena.id}"
}


resource "aws_internet_gateway" "ig_elena" {
  vpc_id = "${aws_vpc.vpc_elena.id}"

  tags = {
    Name = "internet gateway elena"
  }
}

resource "aws_route_table" "rt_elena" {
  vpc_id = "${aws_vpc.vpc_elena.id}"

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = "${aws_internet_gateway.ig_elena.id}"
  }
}

resource "aws_route_table_association" "rt_elena" {
  count = 2
  subnet_id      = "${aws_subnet.elena_ecs.*.id[count.index]}"
  route_table_id = "${aws_route_table.rt_elena.id}"
}