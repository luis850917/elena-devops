resource "aws_ecs_cluster" "cluster_elena" {
  name = "elena cluster" 
  setting {
    name  = "containerInsights"
    value = "enabled"
  }  
}

//task definition

resource "aws_ecs_task_definition" "svc_front" {
  family = "service"
  container_definitions = jsonencode([
    {
      name      = "back"
      image     = ""
      cpu       = 1
      memory    = 256
      essential = true
      portMappings = [
        {
          containerPort = 3000
          hostPort      = 3000
        }
      ]
    },
    {
      name      = "front"
      image     = ""
      cpu       = 1
      memory    = 256
      essential = true
      portMappings = [
        {
          containerPort = 8080
          hostPort      = 8080
        }
      ]
    }
  ]) 

  placement_constraints {
    type       = "memberOf"
    expression = "attribute:ecs.availability-zone in [us-east-2a, us-east-2b]"
  }
}